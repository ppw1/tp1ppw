from django.apps import AppConfig


class BramantaConfig(AppConfig):
    name = 'bramanta'
