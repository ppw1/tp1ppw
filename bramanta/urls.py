from django.urls import path
from .views import *

urlpatterns = [
    path('campaign/', campaignPage, name = 'campaignPage'),
    path('browse/', browseCampaign, name = 'browseCampaign'),
]
