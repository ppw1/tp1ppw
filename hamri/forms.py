from django import forms

from .models import Campaign

class PostCampaign(forms.ModelForm):

    class Meta:
        model = Campaign
        fields = ('Project_name', 'Goal', 'Donation_end_date', 'Description','image')

# class form_sub(forms.Form):
# 	Project_name = forms.CharField(label='', required=True, max_length=40, widget=forms.TextInput(attrs=title_attrs))
#     Goal = forms.CharField(label='', required=True, max_length=40, widget=forms.TextInput(attrs=title_attrs))
# 	Donation_end_date = forms.DateField(label='', required=True, max_length=50, widget=forms.TextInput(attrs=title_attrs))
# 	Description = forms.CharField(label='', required=True, max_length=250, widget=forms.TextInput(attrs=title_attrs))