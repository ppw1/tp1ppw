from django.db import models

# Create your models here.

class Campaign(models.Model):
	Project_name = models.TextField(max_length = 40)
	Goal = models.TextField(max_length = 40)
	Donation_end_date= models.DateField(max_length = 50)
	Description = models.TextField(max_length = 250)
	image=models.FileField(upload_to = 'static/', default = 'static/None/no-img.jpg')
