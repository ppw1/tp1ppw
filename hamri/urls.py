from django.urls import path
from .views import *

urlpatterns = [
    path('createCampaign', createCampaign, name = 'createCampaign'),
    path('savecampaign', savecampaign, name = 'savecampaign')
]