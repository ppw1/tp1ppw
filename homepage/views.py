from django.shortcuts import render
from .forms import form_sub
from django.http import HttpResponseRedirect
# Create your views here.
def index(request):
    return render(request, 'home.html')
