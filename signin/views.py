from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import SignIn
from .models import daftar
from datetime import datetime
# Create your views here.
def register(request):
	
	account = daftar.objects.all().values()
	
	if request.method == 'POST':
		form = SignIn(request.POST)
		if form.is_valid():
			account = daftar()
			account.namadepan = form.cleaned_data['namadepan']
			account.namabelakang  =form.cleaned_dara['namabelakangs']
			account.tgl = form.cleaned_data['tgl']
			account.alamat = form.cleaned_dara['alamat']
			account.email = form.cleaned_data['email']
			account.password = form.cleaned_data['password']
			account.date = datetime.now()
			account.save()
			return HttpResponseRedirect('register')
	else:
		form = SignIn()
	
	response = {
        'form' : form,
        'account' : account,
	}
	
	return render(request, "index.html", response)

