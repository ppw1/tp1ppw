from django.db import models
from datetime import datetime
# Create your models here.
class daftar(models.Model):
    namadepan = models.CharField(max_length = 100)
    namabelakang = models.CharField(max_length = 100)
    alamat = models.CharField(max_length = 100)
    email = models.EmailField(null = True,blank = True)
    tgl = models.DateField()
    password = models.CharField(max_length = 32)
    confirm = models.CharField(max_length = 32)
