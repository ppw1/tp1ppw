# Generated by Django 2.1.1 on 2018-10-20 06:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='daftar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('namadepan', models.CharField(max_length=100)),
                ('namabelakang', models.CharField(max_length=100)),
                ('alamat', models.CharField(max_length=100)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('tanggal_lahir', models.DateField()),
                ('password', models.CharField(max_length=32)),
                ('confirm', models.CharField(max_length=32)),
            ],
        ),
    ]
