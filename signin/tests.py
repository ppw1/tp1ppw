from django.test import TestCase
from .models import daftar
from django.utils import timezone
# Create your tests here.
class Test(TestCase):
    def start(self):
        response = Client.get('/signin/register')
        self.assertEqual(response.status_code,200)

    def canCreate(self):
        newAcc = daftar.objects.create(namadepan = 'Wahyu',namabelakang = 'Agung',alamat = 'Jl. Agung',email = 'randi@gmail.com',tgl = timezone.now(),password = 'biasa')
        counting_all_available_activity = daftar.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

