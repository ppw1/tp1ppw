from django import forms
import datetime

YEARS = [x for x in range(1940,2021)]
class SignIn(forms.Form):
	nama_depan = forms.CharField(label='Nama depan',max_length=100)
	nama_belakang = forms.CharField(max_length = 100)
	alamat = forms.CharField(max_length  =100)
	tgl = forms.DateField(label='Tanggal Lahir',widget=forms.SelectDateWidget(years=YEARS))
	email = forms.EmailField(label='Email',max_length=254)
	password = forms.CharField(label='Password',max_length=100,widget=forms.PasswordInput)
	confirm = forms.CharField(label='Confirm Password',max_length=100,widget=forms.PasswordInput)
